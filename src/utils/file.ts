// 文件工具类
import { app } from 'electron';
import path from 'path';

import fs from 'fs';
import * as fsPromises from 'fs/promises';

const userDataPath = app.getPath('userData');                                         // 用户数据文件的缓存路径
const windowSizeDataFileName = 'window_size.txt';                                     // 窗口大小数据文件名
const defaultWindowSize = '400';                                                      // 窗口默认大小

const windowSizeDataFilePath = path.join(userDataPath, windowSizeDataFileName);       // 窗口数据文件路径

export const sayHello = () => {
  return "hello";
}

// 初始化窗口大小
export const initWindowSize = () => {
    fs.access(windowSizeDataFilePath, fs.constants.F_OK, (err) => {
     if (err) {
      console.log(windowSizeDataFilePath)
       fs.writeFile(windowSizeDataFilePath, defaultWindowSize, { flag: 'wx' }, (err) => {
         if (err) {
          console.log(windowSizeDataFilePath)
           console.error('文件创建或写入时出错:', err);
         } else {
           console.log('文件创建成功并已写入数据。');
         }
       });
     } else {
       console.log('window_size.text is existed');
     }
   });
}

// 从文件读取窗口大小信息
export const readWindowSizeFromFile = async () => {
    try {
      return await fsPromises.readFile(windowSizeDataFilePath, "utf-8")
    } catch (err) {
      console.error(err)
    }
}