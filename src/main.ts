import { app, BrowserWindow, ipcMain, Menu } from 'electron';
import path from 'path';

import { initWindowSize, readWindowSizeFromFile } from './utils/file'
import { sendMessage, receiveMessage } from './utils/UDP'

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit();
}

// 创建并设置菜单栏
const menu = Menu.buildFromTemplate([])
Menu.setApplicationMenu(menu)

const createWindow = () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    receiveMessage();
  }
  initWindowSize()

  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    // titleBarStyle: 'hidden',
    // transparent: true,
    // icon: "",
    // frame: false,
    // transparent: true, // 设置窗口透明
    // 可选：设置背景颜色透明，确保在某些情况下透明效果正常工作
    // backgroundColor: '#00000000',
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  // and load the index.html of the app.
  if (MAIN_WINDOW_VITE_DEV_SERVER_URL) {
    mainWindow.loadURL(MAIN_WINDOW_VITE_DEV_SERVER_URL + '/index.html');
  } else {
    mainWindow.loadFile(path.join(__dirname, `../renderer/${MAIN_WINDOW_VITE_NAME}/index.html`));
  }

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

// 返回读取到的文本数据
ipcMain.handle('loadText', async () => {
  try {
    return await readWindowSizeFromFile()
  } catch (error) {
    console.log(error)
  }
})

// 创建一个副本窗口
ipcMain.handle('create-copy-Window', async () => {
  createWindow();
})

// 关闭一个副本窗口
ipcMain.handle('close-copy-Window', async () => {
  createWindow();
})

// 发送消息
ipcMain.handle('send-message', async () => {
  sendMessage();
})