import { contextBridge, ipcRenderer } from 'electron';

// 返回读取到的文本数据
const loadText = async () => {
    const res = await ipcRenderer.invoke('loadText');
    return res
}

// 创建一个副本窗口
const createCopyWindow = () => {
    ipcRenderer.invoke('create-copy-Window');
}

// 关闭一个副本窗口
const closeCopyWindow = () => {
    ipcRenderer.invoke('close-copy-Window');
}

// 发送消息
const sendMessage = () => {
    ipcRenderer.invoke('send-message');
}

// 将特定的Electron API暴露给渲染进程
contextBridge.exposeInMainWorld('myApi', {
    loadText,
    createCopyWindow,
    closeCopyWindow,
    sendMessage
})
